import { NgModule } from '@angular/core'
import {
    MatButtonModule,
    MatToolbarModule,
    MatTabsModule,
    MatTableModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,

} from '@angular/material'

@NgModule({
    imports: [
        MatFormFieldModule,
        MatButtonModule,
        MatToolbarModule,
        MatTabsModule,
        MatTableModule,
        MatIconModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,


    ],
    exports: [
        MatButtonModule,
        MatToolbarModule,
        MatTabsModule,
        MatTableModule,
        MatIconModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,


    ]
})
export class MaterialModule { }